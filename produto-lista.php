<?php include("cabecalho.php");
	  include("conecta.php");
	  include("banco-produto.php"); ?>

	<?php 
		if (array_key_exists("removido", $_GET) && $_GET['removido'] == "true") {  
	?>
			<div class="text-success">Produto removido com sucesso!</div>
	<?php	
		}
	?>

	<table class="table">
		<thead>
			<tr>
				<th>Produto</th>
				<th>Preço</th>
				<th>Descrição</th>
				<th>Categoria</th>
			</tr>
		</thead>
		<?php  
			$produtos = listaProdutos($conexao);
			foreach($produtos as $produto) :
		?>
				<tbody>
					<tr>
						<td><?=$produto['nome']?></td>
						<td><?="R$ " . $produto['preco']?></td>
						<td><?=substr($produto['descricao'], 0, 10)?></td>
						<td><?=$produto['categoria_nome']?></td>
						<td><a class="btn btn-warning" href="produto-altera-formulario.php?id=<?=$produto['id']?>"> alterar</a></td>
						<td>
							<form action="remove-produto.php" method="POST">
								<input type="hidden" name="id" value="<?=$produto['id']?>"/>
								<button class="btn btn-danger">remover</button> 
							</form>	
						</td>
					</tr>
				</tbody>	
		<?php 
	    	endforeach
	    ?>
    </table>
			
<?php include("rodape.php"); ?>