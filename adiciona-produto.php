<?php include("cabecalho.php");
 	  include("banco-produto.php"); 
 	  include("conecta.php"); 
	 
		$nome = $_POST["nome"]; 
		$preco = $_POST["preco"];
		$descricao = $_POST["descricao"];
		$categoria_id = $_POST["categoria_id"];
		// quando é feito por checkbox terá que fazer uma condiçã ose ele existe pois na hora de cadastrar e ele não for
		// selecionado, ele não será gravado no banco como false
		if (array_key_exists('usado', $_POST)) {
			$usado = "true";
		}else{
			$usado = "false";
		}
		
		if(insereProduto($conexao, $nome, $preco, $descricao, $categoria_id, $usado)){ ?>
				<div class="alert alert-success" role="alert">Produto <?= $nome; ?> adicionado com sucesso!</div>
 <?php  }else{ 
				$msg = mysqli_error($conexao);
 		?>
				<div class="alert alert-danger" role="alert">Produto <?= $nome; ?> não foi adicionado: <?php echo $msg ?></div>
		<?php }

		// não é preciso colocar pq o php automaticamente fecha a conexão
		mysqli_close($conexao);

		?>
<?php include("rodape.php");?>