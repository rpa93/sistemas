<?php include("cabecalho.php");
	  include("conecta.php");
	  include("banco-categoria.php");
	  include("banco-produto.php");

$id = $_GET['id'];
$produto = buscaProduto($conexao, $id);
$categorias = listaCategorias($conexao);
$usado = $produto['usado'] ? "checked='checked'" : "";
?>
			<h1>Alterando produto</h1>
			<form action="altera-produto.php" class="form-horizontal" method="post">
				<input type="hidden" name="id" value="<?=$produto['id']?>" />
				<div class="form-group">
					<label class="control-label col-md-5">Nome:</label>
					<div class="col-md-3">
						<input class="form-control" type="text" name="nome" value="<?=$produto['nome']?>">
					</div>	
				</div>
				<div class="form-group">	
					<label class="control-label col-md-5">Preço:</label>
					<div class="col-md-3">
						<input class="form-control" type="text" name="preco" value="<?=$produto['preco']?>">
					</div>	
				</div>
				<div class="form-group">	
					<label class="control-label col-md-5">Descrição:</label>
					<div class="col-md-3">
						<textarea class="form-control" type="text" name="descricao"><?=$produto['descricao']?></textarea> 
					</div>	
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="usado" <?=$usado?> value="true"> Usado
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-5">Categoria:</label>
					<div class="col-md-3">	
						<select name="categoria_id" class="form-control">
						<?php foreach ($categorias as $categoria): 
							$essaEhACategoria = $produto['categoria_id'] == $categoria['id'];
							$selecao = $essaEhACategoria ? "selected='selected'" : "";
							?>
							<option value="<?=$categoria['id']?>" <?=$selecao?>>
								<?=$categoria['nome']?>
							</option>
						<?php endforeach ?>	
						</select>
					</div>	
				</div>
				<div class="form-group">
					<div class="col-md-offset-1">
						<button class="btn btn-primary" type="submit">Alterar</button>
					</div>	
				</div>	
			</form>
<?php include("rodape.php"); ?>