<?php include("cabecalho.php");
	  include("conecta.php");
	  include("banco-categoria.php");

$categorias = listaCategorias($conexao);
?>
			<h1>Formulário de produto</h1>
			<form action="adiciona-produto.php" class="form-horizontal" method="post">
				<div class="form-group">
					<label class="control-label col-md-5">Nome:</label>
					<div class="col-md-3">
						<input class="form-control" type="text" name="nome">
					</div>	
				</div>
				<div class="form-group">	
					<label class="control-label col-md-5">Preço:</label>
					<div class="col-md-3">
						<input class="form-control" type="text" name="preco">
					</div>	
				</div>
				<div class="form-group">	
					<label class="control-label col-md-5">Descrição:</label>
					<div class="col-md-3">
						<textarea class="form-control" type="text" name="descricao"></textarea> 
					</div>	
				</div>
				<div class="form-group">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="usado" value="true"> Usado
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-5">Categoria:</label>
					<div class="col-md-3">	
						<select name="categoria_id" class="form-control">
						<?php foreach ($categorias as $categoria): ?>
							<option value="<?=$categoria['id']?>">
								<?=$categoria['nome'];?>
							</option>
						<?php endforeach ?>	
						</select>
					</div>	
				</div>
				<div class="form-group">
					<div class="col-md-offset-1">
						<button class="btn btn-primary" type="submit" name="cadastrar">Enviar</button>
					</div>	
				</div>
				
			</form>
<?php include("rodape.php"); ?>